using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Reflection.Metadata.Ecma335;

namespace Module2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Program A = new Program();
            Console.WriteLine(A.GetCongratulation(31));
            Dimensions d = new Dimensions();
            d.FirstSide = 10;
            d.SecondSide = 20;
            d.ThirdSide = 25;
            d.Height = 12;
            d.Diameter = 50;
            d.Radius = 25;
            //Executing the first method
            Console.WriteLine(A.GetCongratulation(30));
            //Executig the second method
            Console.WriteLine(A.GetTotalTax(8848, 666, 99));
            //The implementation of the third method
            Console.WriteLine(A.GetFigureValues(FigureEnum.Triangle, ParameterEnum.Square, d));
            //The implementation of the fourth method
            Console.WriteLine(A.GetMultipliedNumbers("123,456", "1000"));
            Console.WriteLine(A.GetFigureValues(FigureEnum.Circle, ParameterEnum.Square, d));

        }

        public int GetTotalTax(int companiesNumber, int tax, int companyRevenue)
        {
            int result = (companiesNumber * companyRevenue * tax) / 100;
            return result;
        }

        public string GetCongratulation(int input)
        {
            string s;
            if (input % 2 == 0 && input >= 18)
            {
                s = "Поздравляю с совершеннолетием!";
            }
            else if (input % 2 != 0 && input < 18 && input > 12)
            {
                s = "Поздравляю с переходом в старшую школу!";
            }
            else
            {
                s = string.Format("Поздравляю с {0}-летием!", input);
            }

            return s;
        }

        public double GetMultipliedNumbers(string first, string second)
        {
            IFormatProvider formatter = new NumberFormatInfo { NumberDecimalSeparator = "." };
            double n1 = 0;
            double n2 = 0;
            first = first.Replace(',', '.');
            second = second.Replace(',', '.');
            n1 = double.Parse(first, formatter);
            n2 = double.Parse(second, formatter);
            return n1 * n2;
        }

        public double GetFigureValues(FigureEnum figureType, ParameterEnum parameterToCompute, Dimensions dimensions)
        {
            double result = 0;
            switch (figureType)
            {
                case FigureEnum.Circle:
                    {
                        result = (parameterToCompute == ParameterEnum.Perimeter) ? Math.PI * 2 * dimensions.Radius : Math.PI * Math.Pow(dimensions.Radius, 2);
                        result = Math.Truncate(result);
                        break;
                    }
                case FigureEnum.Rectangle:
                    {
                        result = (parameterToCompute == ParameterEnum.Perimeter) ? ((dimensions.FirstSide + dimensions.SecondSide) * 2) : dimensions.FirstSide * dimensions.SecondSide;
                        break;
                    }
                case FigureEnum.Triangle:
                    {
                        if (dimensions.Height != 0) result = 1.0 / 2.0 * dimensions.FirstSide * dimensions.Height;
                        else
                        {
                            double temp = (dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide) * 1.0 / 2;
                            result = Math.Sqrt(temp * (temp - dimensions.FirstSide) * (temp - dimensions.SecondSide) * (temp - dimensions.ThirdSide));
                            result = Math.Truncate(result);
                        }
                        break;
                    }
            }
            return Math.Round(result);
        }
    }
}
